
import feedparser, sqlite3

class PiranhaData:
	def __init__( self, cursor ):
		self.category = {}
		self.feed = {}
		self.entry = {}
		self.tag = {}

		for row in cursor.execute( "select * from category" ):
			self.category[ row["id"] ] = { "id": row["id"], "name": row["name"], "parent": row["parent"], "feeds": [] }

		for row in cursor.execute( "select * from tag" ):
			self.tag[ row["id"] ] = { "id": row["id"], "name": row["name"], "entries": [] }
		
		for row in cursor.execute( "select * from feed" ):
			self.feed[ row["id"] ] = { "id": row["id"], "title": row["title"], "entries": [] }
			self.category[ row["category"] ]["feeds"].append( row["id"] )

		for row in cursor.execute( "select id, feed, sha1, status, stamp, title, author from entry" ):
			self.entry[ row["id"] ] = { "id": row["id"], "status": row["status"], "stamp": row["stamp"], "title": row["title"], "author": row["author"], "content": "" }
			self.feed[ row["feed"] ]["entries"].append( row["id"] )

		for row in cursor.execute( "select * from tagged" ):
			self.tag[ row["tag"] ]["entries"].append( row["entry"] )
	#end __init__

#end PiranhaData

