
import os, sys, getopt
from os import path

import feedparser, sqlite3
import PiranhaData

def version():
	return "0.1"

class Piranha:
	"""Core class for managing a user's feeds, caching/accessing feed items, etc."""

	def __init__( self, home=None ):
		self._home = home
		self._db = None

		self.check_home()
		self.check_db()

		self._data = PiranhaData.PiranhaData( self.db() )

	#### class management functions ####

	def db( self, isolation=None ):
		"""Return a new cursor to the database.  This will connect to the database if needed."""
		if isolation == None:
			if self._db == None:
				db = sqlite3.connect( self._dbfile, isolation_level=isolation )
				self._db = db
			else:
				db = self._db
		else:
			db = sqlite3.connect( self._dbfile, isolation_level=isolation )

		db.row_factory = sqlite3.Row
		return db
	#end db()

	def check_home( self ):
		"""Checks the existance of the user's Pirhanna directory and db file.
		Creates both if they do not already exist."""

		if None == self._home:
			self._home = path.join( path.expanduser("~"), ".piranha" )
		self._dbfile = path.join( self._home, "piranha.db" )

		if not path.isdir( self._home ):
			os.mkdir( self._home )
	#end check_home()

	def check_db( self ):
		"""Check the internal schema of any sqlite databases to make sure they are the correct version."""
		db = self.db()
		cursor = db.cursor()

		# Check the existance of the config table
		found = False
		for row in cursor.execute( "select name from sqlite_master where type='table' and name='config'" ):
			if row["name"] == "config":
				found = True

		# Create the config table if needed
		if not found:
			cursor.execute( "create table config( name text unique, ival int, rval real, tval text )" )
			cursor.execute( "insert into config( name, ival ) values ( 'schema', 0 )" )
		
		# Check the database version
		current = 0
		latest = len(self._schema)
		for row in cursor.execute( "select ival from config where name='schema'" ):
			current = row["ival"]

		# Update the database as necessary
		if current < latest:
			lockdb = self.db("EXCLUSIVE")
			while current < latest:
				try:
					lockdb.execute( self._schema[current] )
				except sqlite3.IntegrityError:
					lockdb.execute( "rollback" )
					raise
				current = current + 1

			lockdb.execute( "update config set ival=? where name='schema'", (latest,) )
			lockdb.commit() #done
			
			# invalidate cached connection
			self._db = None
	#end check_db()

	"""Basic schema for the piranha.db file."""
	_schema = [
		'''create table category ( id integer primary key autoincrement, name text, parent integer )''',
		'''create table feed ( id integer primary key autoincrement, category integer, url text unique, title text )''',
		'''create table entry ( id integer primary key autoincrement, feed integer, sha1 text, status integer, stamp text, title text, author text, content blob )''',
		'''create table tag ( id integer primary key autoincrement, name text unique )''',
		'''create table tagged ( entry integer, tag integer )''',
		]
#end Pirhanna

if __name__ == "__main__":
	"""Piranha is a backend, so by default, do nothing."""
	pass
