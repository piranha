#!/usr/bin/env python

import os, sys, getopt, re
import Piranha

def main():
	print "Piranha CLI"
	try:
		opts, args = getopt.gnu_getopt( sys.argv[1:], "v" )
	except getopt.GetoptError:
		sys.exit(2)

	for ( opt, arg ) in opts:
		if opt == "-v":
			print "Pirhanna! Version " + Piranha.version()
			sys.exit(0)
	
	piranha = Piranha.Piranha()

	more = True
	while more:
		try:
			line = raw_input( "P> " )
		except EOFError:
			print "\nGoodbye."
			more = False
			continue

		command = parse( line )
		if command == None:
			print "Unrecognized input."
			continue
		if command[0] == "quit":
			more = False
			continue
		elif command[0] == "list":
			print command
		else:
			print "Invalid command."
			print command
				
def parse( line ):
	parser = re.compile( "^([a-zA-Z]+)( [a-zA-Z0-9:]+)*" )
	matches = parser.search( line )
	command = []

	if matches.lastindex == None:
		return None

	print matches
	for i in range( 1, matches.lastindex+1 ):
		command.append(matches.group(i))

	return command
	
if __name__ == "__main__":
	main()
